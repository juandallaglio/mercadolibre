package dallaglio.juan.meli

import android.app.Application
import timber.log.Timber

class InitApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}