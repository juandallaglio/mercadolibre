package dallaglio.juan.meli.data.network

import com.google.gson.GsonBuilder
import dallaglio.juan.meli.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkFactory {

    private val loginInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)


    open fun <S> createService (serviceClass : Class<S>):S{
        return createService(serviceClass, BuildConfig.URL)
    }

    private fun <S> createService (serviceClass : Class<S>, url:String) : S{
        val builder = OkHttpClient().newBuilder()
        builder.addInterceptor(loginInterceptor)

        return Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(serviceClass)
    }


}
