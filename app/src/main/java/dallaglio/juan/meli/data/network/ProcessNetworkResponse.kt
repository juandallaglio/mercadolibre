package dallaglio.juan.meli.data.network

import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.util.concurrent.CancellationException

suspend fun <ResponseType, ResultType> processedNetworkResource (
    createCall: suspend () -> Response<ResponseType>,
    processResponse: (response: ResponseType) -> ResultType
) : Resource<ResultType> {
    return try {
        Resource.loading(null)
        val apiResponse = createCall()
        when {
            apiResponse.isSuccessful -> {
                Resource.success(apiResponse.body()?.let { processResponse(it) })
            }
            else -> {
                Timber.d(apiResponse.toString())
                Resource.error(apiResponse.message(), null)
            }
        }
    } catch (e: HttpException) {
        Timber.d(e)
        Resource.error(e.message.orEmpty(),  null)
    } catch (e: CancellationException){
        Timber.d(e)
       Resource.success(null)
    } catch (e: Exception) {
        Timber.d(e)
        Resource.error(e.message.orEmpty(), null)
    }
}