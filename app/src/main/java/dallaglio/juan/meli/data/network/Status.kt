package dallaglio.juan.meli.data.network

enum class Status {
    SUCCESS, ERROR, LOADING
}