package dallaglio.juan.meli.data.network.dto

import com.google.gson.annotations.SerializedName

data class SearchQueryResponse(
    @SerializedName("results")
    val results : List<Item>
)

data class Item(
    @SerializedName("id")
    val id : String,
    @SerializedName("title")
    val title : String,
    @SerializedName("price")
    val price : Double,
    @SerializedName("currency_id")
    val currency: String,
    @SerializedName("condition")
    val condition : String,
    @SerializedName("thumbnail")
    val thumbnailUrl: String
)