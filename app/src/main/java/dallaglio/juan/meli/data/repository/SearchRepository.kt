package dallaglio.juan.meli.data.repository

import dallaglio.juan.meli.data.network.NetworkFactory
import dallaglio.juan.meli.data.network.Resource
import dallaglio.juan.meli.data.network.dto.Item
import dallaglio.juan.meli.data.network.processedNetworkResource
import dallaglio.juan.meli.data.repository.api.IApiMeliService

interface ISearchRepository {
    suspend fun searchQuery(search: String): Resource<List<Item>>
}

class SearchRepository: ISearchRepository {

    private val service = NetworkFactory().createService(IApiMeliService::class.java)

    override suspend fun searchQuery(search: String): Resource<List<Item>> {
       return processedNetworkResource({
           service.searchQuery(search)
       },{ searchQueryResponse ->
           searchQueryResponse.results
       })
    }
}