package dallaglio.juan.meli.data.repository.api

import dallaglio.juan.meli.data.network.dto.SearchQueryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IApiMeliService {

    @GET("/sites/MLA/search")
    suspend fun searchQuery(@Query("q") search:String) : Response<SearchQueryResponse>

}