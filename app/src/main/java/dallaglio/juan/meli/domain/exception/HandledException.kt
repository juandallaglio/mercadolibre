package dallaglio.juan.meli.domain.exception

sealed class HandledException(override var message:String?) : Throwable()
class CoroutineCancelException : HandledException("")

