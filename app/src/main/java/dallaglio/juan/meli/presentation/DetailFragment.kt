package dallaglio.juan.meli.presentation

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import dallaglio.juan.meli.R
import dallaglio.juan.meli.presentation.base.BaseFragment
import dallaglio.juan.meli.utils.condition
import dallaglio.juan.meli.utils.loadHigh
import dallaglio.juan.meli.utils.moneyCurrency
import dallaglio.juan.meli.utils.moneySymbol
import dallaglio.juan.meli.viewmodel.ShareViewModel

class DetailFragment : BaseFragment(R.layout.fragment_detail){

    private val shareViewModel: ShareViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemImage = view.findViewById<ImageView>(R.id.image)
        val itemId = view.findViewById<TextView>(R.id.item_id)
        val itemTitle = view.findViewById<TextView>(R.id.item_title)
        val itemSubTitle = view.findViewById<TextView>(R.id.item_subtitle)

        shareViewModel.itemSelect.observe(viewLifecycleOwner, { item ->
            item?.let {
                itemImage.loadHigh(it.thumbnailUrl)
                itemId.text = it.id
                itemTitle.text = it.title
                itemSubTitle.text = "${item.currency.moneySymbol()} ${item.price.moneyCurrency()} -  ${item.condition.condition()}"
            } ?: findNavController().navigateUp()
        })
    }
}