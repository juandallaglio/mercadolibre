package dallaglio.juan.meli.presentation

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dallaglio.juan.meli.R
import dallaglio.juan.meli.data.network.dto.Item
import dallaglio.juan.meli.presentation.adapter.ItemAdapter
import dallaglio.juan.meli.presentation.base.BaseFragment
import dallaglio.juan.meli.presentation.base.hideKeyboard
import dallaglio.juan.meli.viewmodel.MainViewModel
import dallaglio.juan.meli.viewmodel.ShareViewModel
import timber.log.Timber

class SearchFragment : BaseFragment(R.layout.fragment_search), SearchView.OnQueryTextListener {

    private val mainViewModel: MainViewModel by navGraphViewModels(R.id.nav_graph)
    private val shareViewModel: ShareViewModel by activityViewModels()

    lateinit var progressBar: ProgressBar
    lateinit var recycler: RecyclerView
    lateinit var searchBar : SearchView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBar = view.findViewById(R.id.progress)
        recycler = view.findViewById(R.id.recycler)
        searchBar = view.findViewById(R.id.search)

        searchBar.setOnQueryTextListener(this)
        searchBar.isIconified = false;
        searchBar.requestFocusFromTouch();

        recycler.layoutManager = LinearLayoutManager(requireContext())

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if(newState == RecyclerView.SCROLL_STATE_DRAGGING || newState ==RecyclerView.SCROLL_STATE_SETTLING)
                    hideKeyboard()
            }
        })

        mainViewModel.isLoading.observe(viewLifecycleOwner, { loading ->
            showLoading(loading)
        })

        mainViewModel.error.observe(viewLifecycleOwner, { error ->
            error?.let { alertDialogError(message = it) }
        })

        mainViewModel.list.observe(viewLifecycleOwner, { list ->
            list?.let {
                val adapter = ItemAdapter(it)
                recycler.adapter = adapter
                adapter.onItemClick = { item -> itemClick(item)}
            }
        })
        mainViewModel.search("telefono")
        hideKeyboard()
    }

    private fun itemClick(item:Item){
        Timber.d(item.toString())
        shareViewModel.setSelectItem(item)
        findNavController().navigate(R.id.action_searchFragment_to_detailFragment)
    }

    private fun showLoading(b: Boolean) {
        progressBar.visibility = if (b) View.VISIBLE else View.INVISIBLE
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        hideKeyboard()
        query?.let { mainViewModel.search(query) }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let { mainViewModel.search(newText) }
        return true
    }

}