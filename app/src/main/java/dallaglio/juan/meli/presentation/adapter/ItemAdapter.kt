package dallaglio.juan.meli.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dallaglio.juan.meli.R
import dallaglio.juan.meli.data.network.dto.Item
import dallaglio.juan.meli.utils.condition
import dallaglio.juan.meli.utils.loadLow
import dallaglio.juan.meli.utils.moneyCurrency
import dallaglio.juan.meli.utils.moneySymbol

class ItemAdapter(var list : List<Item>) : RecyclerView.Adapter<ItemAdapter.ItemAdapterHolder>() {

    var onItemClick: ((Item) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapterHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_adapter_item, parent, false)
        return ItemAdapterHolder(view)
    }

    override fun onBindViewHolder(holder: ItemAdapterHolder, position: Int) {
        holder.itemDivisor.visibility =  if(list.size == position) View.INVISIBLE else View.VISIBLE
        val item = list[position]

        holder.itemTitle.text = item.title
        holder.itemId.text = item.id
        holder.itemSubtitle.text = "${item.currency.moneySymbol()} ${item.price.moneyCurrency()} -  ${item.condition.condition()}"
        holder.itemImage.loadLow(item.thumbnailUrl)
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(item)
        }
    }

    override fun getItemCount(): Int {
       return list.size
    }


    class ItemAdapterHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView = itemView.findViewById(R.id.image)
        var itemId: TextView = itemView.findViewById(R.id.item_id)
        var itemTitle: TextView = itemView.findViewById(R.id.item_title)
        var itemSubtitle: TextView = itemView.findViewById(R.id.item_subtitle)
        var itemDivisor: View = itemView.findViewById(R.id.view)
    }

}
