package dallaglio.juan.meli.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import dallaglio.juan.meli.R
import java.util.*

fun String.moneySymbol() : String{
    return Currency.getInstance(this).symbol
}
fun Double.moneyCurrency(): String{
    return String.format("%1\$,.2f", this)
}
fun String.condition() : String{
    return when (this) {
        "new" -> "Nuevo"
        "used" -> "Usado"
        else -> ""
    }
}
fun ImageView.loadLow(url:String){
    Glide.with(this)
        .load(url)
        .centerCrop()
        .thumbnail(.3F)
        .placeholder(R.drawable.ic_launcher_foreground)
        .error(R.drawable.ic_launcher_foreground)
        .into(this)
}

fun ImageView.loadHigh(url:String){
    Glide.with(this)
        .load(url)
        .centerInside()
        .thumbnail(.8F)
        .placeholder(R.drawable.ic_launcher_foreground)
        .error(R.drawable.ic_launcher_foreground)
        .into(this)
}