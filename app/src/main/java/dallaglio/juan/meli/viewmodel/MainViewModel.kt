package dallaglio.juan.meli.viewmodel

import androidx.lifecycle.*
import dallaglio.juan.meli.data.network.Resource
import dallaglio.juan.meli.data.network.Status
import dallaglio.juan.meli.data.network.dto.Item
import dallaglio.juan.meli.data.repository.ISearchRepository
import dallaglio.juan.meli.data.repository.SearchRepository
import dallaglio.juan.meli.domain.exception.CoroutineCancelException
import kotlinx.coroutines.*
import timber.log.Timber
import java.util.concurrent.CancellationException

class MainViewModel : ViewModel() {

    private var searchJob: Job? = null

    private val repository: ISearchRepository = SearchRepository()

    private val _isLoading = MutableLiveData(false)
    var isLoading: LiveData<Boolean> = _isLoading

    private val _error = MutableLiveData<String>()
    var error: LiveData<String> = _error

    private val _list = MutableLiveData<List<Item>>()
    var list: LiveData<List<Item>> = _list

    fun search(text: String) {
        if (text.isEmpty() || text.length < 3)
            return
        if (searchJob != null && searchJob!!.isActive) searchJob?.cancel()
        showLoading()
        searchJob = viewModelScope.launch {
            try {
                val result = repository.searchQuery(text)
                when (result.status) {
                    Status.SUCCESS -> {
                        hideLoading()
                        result.data?.let { _list.value = it }
                    }
                    Status.ERROR -> {
                        hideLoading()
                        result.message?.let { _error.value = it }
                    }
                }
            } catch (e:CancellationException){
                Timber.d(e)
            }
        }
    }


    private fun showLoading() {
        _isLoading.value = true
    }

    private fun hideLoading() {
        _isLoading.value = false
    }

}

