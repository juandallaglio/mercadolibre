package dallaglio.juan.meli.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dallaglio.juan.meli.data.network.dto.Item

class ShareViewModel : ViewModel() {

    private val _item = MutableLiveData<Item>()
    val itemSelect : LiveData<Item> = _item

    fun setSelectItem(item:Item){
        _item.value = item
    }

}