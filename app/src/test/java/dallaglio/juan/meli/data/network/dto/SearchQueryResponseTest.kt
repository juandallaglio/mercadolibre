package dallaglio.juan.meli.data.network.dto

import org.junit.Test
import org.junit.Assert.*
import org.junit.Before

class SearchQueryResponseTest{

    private lateinit var item : Item
    private lateinit var query : SearchQueryResponse

    @Before
    fun before(){
        item = Item("123", "title", 123.00, "USD", "New", "photoUrl")
        query = SearchQueryResponse(arrayOf(item, item, item).toList())
    }

    @Test
    fun dtoItem(){

        assertEquals("123", item.id)
        assertEquals( "title",item.title)
        assertEquals( 123.00,item.price, 0.00)
        assertEquals( "USD",item.currency)
        assertEquals( "New",item.condition)
        assertEquals( "photoUrl",item.thumbnailUrl)
    }

    @Test
    fun dtoSearchQueryResponse(){
        assert(query.results is List<Item>)
        for (it in query.results) {
            assertEquals("123", it.id)
            assertEquals( "title",it.title)
            assertEquals( 123.00,it.price, 0.00)
            assertEquals( "USD",it.currency)
            assertEquals( "New",it.condition)
            assertEquals( "photoUrl",it.thumbnailUrl)
        }
    }
}