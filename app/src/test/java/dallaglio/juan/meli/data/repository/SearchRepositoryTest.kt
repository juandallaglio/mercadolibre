package dallaglio.juan.meli.data.repository

import dallaglio.juan.meli.data.network.Resource
import dallaglio.juan.meli.data.network.Status
import dallaglio.juan.meli.data.network.dto.Item
import dallaglio.juan.meli.data.network.dto.SearchQueryResponse
import dallaglio.juan.meli.data.repository.api.IApiMeliService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import retrofit2.Response


class SearchRepositoryTest {

    @MockK
    lateinit var mockRepository: ISearchRepository

    @MockK
    lateinit var service: IApiMeliService

    private lateinit var item : Item
    private lateinit var query : SearchQueryResponse
    private val searchString = "searchWord"
    private val errorMsg = "error message"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        item = Item("123", "title", 123.00, "USD", "New", "photoUrl")
        query = SearchQueryResponse(arrayOf(item, item, item).toList())

    }

    @Test
    fun `test search status loading`(){
        coEvery { mockRepository.searchQuery(searchString) } returns Resource.loading( null)

        runBlocking {
            val resRepo = mockRepository.searchQuery(searchString)
            assertEquals(Status.LOADING, resRepo.status)
        }

    }
    @Test
    fun `test search status success`(){
        coEvery { mockRepository.searchQuery(searchString) } returns Resource.success(null)

        runBlocking {
            val resRepo = mockRepository.searchQuery(searchString)
            assertEquals(Status.SUCCESS, resRepo.status)
        }

    }
    @Test
    fun `test search status error`(){
        coEvery { mockRepository.searchQuery(searchString) } returns Resource.error("null", null)

        runBlocking {
            val resRepo = mockRepository.searchQuery(searchString)
            assertEquals(Status.ERROR, resRepo.status)
        }

    }

    @Test
    fun `test search repository`(){
        coEvery { mockRepository.searchQuery(searchString) } returns Resource.success(query.results)

        runBlocking {
            val resRepo = mockRepository.searchQuery(searchString)
            assertEquals(query.results, resRepo.data)
        }

    }

    @Test
    fun `test search service`(){
        coEvery { service.searchQuery(searchString) } returns Response.success(query)

        runBlocking {
            val resService = service.searchQuery(searchString)
            assertEquals(query, resService.body())
        }

    }

    @Test
    fun `test search repository exception`(){
        coEvery { mockRepository.searchQuery(searchString) } returns Resource.error(errorMsg, null)

        runBlocking {
            val resRepo = mockRepository.searchQuery(searchString)
            assertEquals(errorMsg, resRepo.message)
        }

    }




}